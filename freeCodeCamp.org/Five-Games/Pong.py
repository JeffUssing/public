# Simple Pong in Python 3 for Beginners
# Part 1: Getting Started

import turtle
import time
import winsound

wn = turtle.Screen()
wn.title("Pong")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(0)

# Score
score_a = 0
score_b = 0

# Paddle A
pdl_a = turtle.Turtle()
pdl_a.speed(0)
pdl_a.shape("square")
pdl_a.color("white")
pdl_a.shapesize(stretch_wid=5, stretch_len=1)
pdl_a.penup()
pdl_a.goto(-350, 0)

# Paddle B
pdl_b = turtle.Turtle()
pdl_b.speed(0)
pdl_b.shape("square")
pdl_b.color("white")
pdl_b.shapesize(stretch_wid=5, stretch_len=1)
pdl_b.penup()
pdl_b.goto(350, 0)

# Ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("circle")
ball.color("white")
ball.penup()
ball.goto(0, 0)
ball.state = 0
ball.dx = -1
ball.dy = 1

# Pen
pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Player A:0  Player B:0", align="center", font=("Courier", 24, "normal"))


# Functions
def pdl_a_up():
    y = pdl_a.ycor()
    y = y + 20
    pdl_a.sety(y)


def pdl_a_dn():
    y = pdl_a.ycor()
    y -= 20
    pdl_a.sety(y)


def pdl_b_up():
    y = pdl_b.ycor()
    y += 20
    pdl_b.sety(y)


def pdl_b_dn():
    y = pdl_b.ycor()
    y -= 20
    pdl_b.sety(y)


def bounce():
    ball.dy *= -1
    winsound.PlaySound("Pong Blip A5.wav", winsound.SND_FILENAME | winsound.SND_ASYNC)


def score(plr):
    global score_a
    global score_b
    winsound.PlaySound("Pong Blip A3.wav", winsound.SND_FILENAME | winsound.SND_ASYNC)
    ball.color("red")
    wn.update()
    time.sleep(0.5)
    ball.state = 0
    if plr == "A":
        score_a += 1
    if plr == "B":
        score_b += 1
    pen.clear()
    pen.write("Player A:{}  Player B:{}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))


# Keyboard binding
wn.listen()
wn.onkeypress(pdl_a_up, "w")
wn.onkeypress(pdl_a_dn, "s")
wn.onkeypress(pdl_b_up, "Up")
wn.onkeypress(pdl_b_dn, "Down")

# Main game loop
while True:
    wn.update()

    # Check the ball's state
    if ball.state == 0:
        ball.color("white")
        ball.goto(0, 0)
        ball.dx *= -1
        wn.update()
        ball.state = 1
        time.sleep(0.5)

    # Move the ball
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)
    time.sleep(.0075)

    # Border check
    # Bounce top
    if ball.ycor() > 290:
        ball.sety(290)
        bounce()
        continue

    # Bounce bottom
    if ball.ycor() < -285:
        ball.sety(-285)
        bounce()
        continue

    # Score left (A)
    if ball.xcor() < -387:
        score("B")
        continue

    # Score right (B)
    if ball.xcor() > 380:
        score("A")
        continue

    # Paddle/ball collisions
    # Paddle left (A)
    if -335 < ball.xcor() < -330:
        pdl_top = pdl_a.ycor() + 55
        pdl_btm = pdl_a.ycor() - 55
        if pdl_top > ball.ycor() > pdl_btm:
            ball.dx *= -1
            winsound.PlaySound("Pong Blip A4.wav", winsound.SND_FILENAME | winsound.SND_ASYNC)

    # Paddle right (B)
    if 330 < ball.xcor() < 335:
        pdl_top = pdl_b.ycor() + 55
        pdl_btm = pdl_b.ycor() - 55
        if pdl_top > ball.ycor() > pdl_btm:
            ball.dx *= -1
            winsound.PlaySound("Pong Blip A4.wav", winsound.SND_FILENAME | winsound.SND_ASYNC)
